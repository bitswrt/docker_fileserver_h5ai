FROM ubuntu:latest
MAINTAINER Deukjin Kim <kdalma@kimssoft.com>

ENV DEBIAN_FRONTEND noninteractive
ENV HTTPD_USER www-data

RUN apt-get update && apt-get install -y \
  nginx php5-fpm supervisor \
  wget unzip patch acl

# install h5ai and patch configuration
COPY h5ai.tar h5ai.tar
RUN tar xvf h5ai.tar && mv h5ai /usr/share/

# patch h5ai because we want to deploy it ouside of the document root and use /var/www as root for browsing
COPY App.php.patch App.php.patch
RUN patch -p1 -u -d /usr/share/h5ai/_h5ai/server/php/inc/ -i /App.php.patch && rm App.php.patch

RUN rm /etc/nginx/sites-enabled/default

#make the cache writable
RUN chmod 777 /usr/share/h5ai/_h5ai/cache/
COPY nginx.conf /etc/nginx/sites-enabled/h5.conf
COPY fileupload.py /var/www/fileupload.py

# use supervisor to monitor all services
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
CMD supervisord -c /etc/supervisor/conf.d/supervisord.conf

# expose only nginx HTTP port
VOLUME ["/var/www"]
EXPOSE 80 443
