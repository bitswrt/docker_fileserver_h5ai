h5ai fileserver 
=================

Simple File Server(h5ai-0.27.0) in a Docker container; based on Ubuntu 14.04.


Usage
-----

    sudo docker run -d \
    --name fileserver \
    -p [service port number]:80 \
    -v [file directory]:/var/www \
    service/fileserver


Sample
------

    sudo docker run -d \
    --name fileserver \
    -p 8009:80 \
    -v ~/tcs/fileserver:/var/www \
    service/fileserver


Building
--------

    sudo docker build -t service/fileserver --rm=true .


