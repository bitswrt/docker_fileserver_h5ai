#!/usr/bin/env python

import sys
import itertools
import mimetools
import mimetypes
from cStringIO import StringIO
import urllib
import urllib2

class MultiPartForm(object):
    """Accumulate the data to be used when posting a form."""

    def __init__(self):
        self.form_fields = []
        self.files = []
        self.boundary = mimetools.choose_boundary()
        return

    def get_content_type(self):
        return 'multipart/form-data; boundary=%s' % self.boundary

    def add_field(self, name, value):
        """Add a simple field to the form data."""
        self.form_fields.append((name, value))
        return

    def add_file(self, fieldname, filename, fileHandle, mimetype=None):
        """Add a file to be uploaded."""
        body = fileHandle.read()
        if mimetype is None:
            mimetype = mimetypes.guess_type(filename)[0] or 'application/octet-stream'
        self.files.append((fieldname, filename, mimetype, body))
        return

    def __str__(self):
        """Return a string representing the form data, including attached files."""
        # Build a list of lists, each containing "lines" of the
        # request.  Each part is separated by a boundary string.
        # Once the list is built, return a string where each
        # line is separated by '\r\n'.
        parts = []
        part_boundary = '--' + self.boundary

        # Add the form fields
        parts.extend(
            [ part_boundary,
              'Content-Disposition: form-data; name="%s"' % name,
              '',
              value,
            ]
            for name, value in self.form_fields
            )

        # Add the files to upload
        parts.extend(
            [ part_boundary,
              'Content-Disposition: file; name="%s"; filename="%s"' % (field_name, filename),
              'Content-Type: %s' % content_type,
              '',
              body,
            ]
            for field_name, filename, content_type, body in self.files
            )

        # Flatten the list and add closing boundary marker,
        # then return CR+LF separated data
        flattened = list(itertools.chain(*parts))
        flattened.append('--' + self.boundary + '--')
        flattened.append('')
        return '\r\n'.join(flattened)


if __name__ == '__main__':

    if len(sys.argv) < 3:
        print "------------------------------------------------------------------"
        print "Usage : fileupload.py [host path] [local file path]"
        print "ex) fileupload.py http://tcs.lge.com:8000/build/test ~/work/test.png"
        print "------------------------------------------------------------------"

    else:

        host_path = sys.argv[1]
        local_path = sys.argv[2]
        url = None
        href = None
        filepath = None
        filename = None

        index = host_path.find("://")
        if index > 0:
            index = host_path.find("/", index+3)
            if index > 0:
                url = host_path[:index]
                href = host_path[index:]
        if url is None:
            url = host_path
            href = "/"

        filepath = local_path
        index = local_path.rfind("/")
        if index >=0:
            filename = local_path[index+1:]
        else:
            filename = local_path


        form = MultiPartForm()
        form.add_field("action", "upload")
        form.add_field("href", href)
        form.add_file('userfile', filename, fileHandle=StringIO(filepath))

        request = urllib2.Request(url)
        request.add_header('User-agent', 'TCS (http://tcs.lge.com)')
        body = str(form)
        request.add_header('Content-type', form.get_content_type())
        request.add_header('Content-length', len(body))
        request.add_data(body)

        print "====================================="
        print "* url      : %s" % url
        print "* path     : %s" % href
        print "* filepath : %s" % filepath
        print "* filename : %s" % filename
        print "====================================="

        print urllib2.urlopen(request).read()


